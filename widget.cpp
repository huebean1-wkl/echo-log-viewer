#include "widget.h"
#include "ui_widget.h"
#include <QTableWidget>
#include <QDebug>
#include <QXmlStreamReader>
#include <QFile>
#include <QFileDialog>
#include <QStandardPaths>
#include <QList>
#include <QMessageBox>
#include <QDirIterator>
#include <QStringList>
#include <QSizeGrip>
#include <QShortcut>
#include <QApplication>
#include <QProcess>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    //Win 10 fix for missing line under column headers
    setStyleSheet(
                "QHeaderView::section{"
                "border-top:0px solid #D8D8D8;"
                "border-left:0px solid #D8D8D8;"
                "border-right:1px solid #D8D8D8;"
                "border-bottom: 1px solid #D8D8D8;"
                "background-color:white;"
                "padding:4px;"
                "}"
                "QTableCornerButton::section{"
                "border-top:0px solid #D8D8D8;"
                "border-left:0px solid #D8D8D8;"
                "border-right:1px solid #D8D8D8;"
                "border-bottom: 1px solid #D8D8D8;"
                "background-color:white;"
                "}");


    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);

    QFont titleFont( "Arial", 16, QFont::Bold);
    ui->label->setFont(titleFont);

    showLegend(false);

    ui->radioButton_DMSO->setEnabled(false);
    ui->radioButton_Volume->setEnabled(false);
    ui->button_relations->setEnabled(false);
    ui->button_survey->setEnabled(false);

    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setRowCount(1);
    ui->tableWidget->horizontalHeader()->hide();
    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->setItem(0, 0, new QTableWidgetItem);
    ui->tableWidget->item(0, 0)->setText("Click to select log files");

    QFont welcomeFont( "Arial", 40, QFont::Bold);
    ui->tableWidget->item(0,0)->setFont(welcomeFont);
    ui->tableWidget->item(0,0)->setTextAlignment(Qt::AlignCenter);

    ui->pushButton->hide();

    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q), this, SLOT(close()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this, SLOT(on_pushButton_clicked()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_R), this, SLOT(restart()));
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_I), this, SLOT(info()));
    new QShortcut(QKeySequence(Qt::Key_D), this, SLOT(on_radioButton_DMSO_clicked()));
    new QShortcut(QKeySequence(Qt::Key_V), this, SLOT(on_radioButton_Volume_clicked()));

}

void Widget::parseLog(QString path){

    ui->tableWidget->horizontalHeader()->show();
    ui->tableWidget->verticalHeader()->show();

    caluclateMinMax(path);
    surveyExport = "";
    dmsoCSV = "";

    if(ui->radioButton_Volume->isChecked()){
        ui->labelMax->setText(QString::number(Vmax));
        ui->labelMin->setText(QString::number(Vmin));
    }else if (ui->radioButton_DMSO->isChecked()){
        ui->labelMax->setText(QString::number(Dmax));
        ui->labelMin->setText(QString::number(Dmin));
    }

    //Open XML file
    QFile file(path);
    QXmlStreamReader reader(&file);

    if(!file.open(QFile::ReadOnly | QFile::Text)){
        qDebug() << "Cannot read file" << file.errorString();
        exit(0);
    }

    //Survey files:
    if (reader.readNextStartElement()) {
        if (reader.name() == "platesurvey"){

            ui->radioButton_DMSO->setEnabled(true);
            ui->radioButton_Volume->setEnabled(true);
            ui->button_relations->setEnabled(true);
            ui->button_survey->setEnabled(true);

            barcode = reader.attributes().at(1).value().toString();
            ui->label->setText("Survey: " + barcode);

            surveyExport.append("Survey: Source " + barcode + "\n\n");
            showLegend(true);

            if(reader.attributes().at(0).value().toString().contains("1536")){
                rows = 32;
                columns = 48;
            }else if (reader.attributes().at(0).value().toString().contains("384")){
                rows = 16;
                columns = 24;
            }else{
                qDebug() << "Unknown well number in source plate name";
            }

            //Fill table with empty QTableWidgetItems
            ui->tableWidget->setColumnCount(columns);
            ui->tableWidget->setRowCount(rows);

            QStringList headers = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF"};
            ui->tableWidget->setVerticalHeaderLabels(headers);

            for (int i = 0; i < rows; i++){
                for (int j = 0; j < columns; j++){
                    ui->tableWidget->setItem(i, j, new QTableWidgetItem);
                    ui->tableWidget->item(i, j)->setBackground(Qt::white);
                }
            }

            //Get volume and % DMSO attributes
            while(reader.readNext()){
                if(reader.atEnd()) break;

                if(reader.name() == "w"){
                    int row = reader.attributes().at(0).value().toInt();
                    int col = reader.attributes().at(1).value().toInt();
                    QString vol = reader.attributes().at(3).value().toString();
                    QString DMSO = reader.attributes().at(11).value().toString();
                    double volume = reader.attributes().at(3).value().toDouble();
                    double DMSOcontent = reader.attributes().at(11).value().toDouble();

                    double value = 1.0;

                    //Calculation for heatmap
                    if(ui->radioButton_Volume->isChecked()){
                        if(Vmax > Vmin){
                            value = ((volume - Vmin) / (Vmax - Vmin));
                        }else{
                            value = 0;
                        }
                        ui->tableWidget->item(row, col)->setToolTip(vol);
                    }else if (ui->radioButton_DMSO->isChecked()){
                        if(Dmax > Dmin){
                            value = ((DMSOcontent - Dmin) / (Dmax - Dmin));
                        }else{
                            if(DMSOcontent > 0){
                                value = 0;
                            }else{
                                value = -1;
                            }
                        }
                        ui->tableWidget->item(row, col)->setToolTip(DMSO);
                    }

                    //Change cell colors
                    ui->tableWidget->item(row, col)->setBackgroundColor(Qt::white);
                    if(value >= 0){
                        ui->tableWidget->item(row, col)->setBackgroundColor(QColor::fromHsv(100-value*100, 255, 255));
                    }
                }
                reader.readNext();
            }

            if(ui->radioButton_Volume->isChecked()){
                surveyExport.append("Volume in uL:\n");
            }else{
                surveyExport.append("DMSO content in %:\n");
            }

            for(int i = 0; i < ui->tableWidget->rowCount(); i++){
                for(int j = 0; j < ui->tableWidget->columnCount(); j++){
                    if(ui->tableWidget->item(i, j)->toolTip() == ""){
                        surveyExport.append("-");
                    }else{
                        surveyExport.append(ui->tableWidget->item(i, j)->toolTip());
                    }
                    surveyExport.append(";");
                    if((j+1) % columns == 0){
                        surveyExport.append("\n");
                    }
                }
            }


            //Transfer files:
        }else if (reader.name() == "transfer"){
            showLegend(false);
            ui->button_relations->setEnabled(true);
            ui->radioButton_DMSO->setEnabled(false);
            ui->radioButton_Volume->setEnabled(false);
            ui->button_survey->setEnabled(false);

            while(reader.readNextStartElement()){
                //Get source and destination barcodes + plate format
                int sourceFormat = 0;
                int destinationFormat = 0;
                if(reader.name() == "plateInfo"){
                    while(reader.readNextStartElement()){
                        if(reader.attributes().at(0).value().toString() == "source"){
                            source = reader.attributes().at(2).value().toString();
                            if(reader.attributes().at(1).value().contains("1536")){
                                sourceFormat = 1536;
                            }else if (reader.attributes().at(1).value().contains("384")){
                                sourceFormat = 384;
                            }
                        }else if(reader.attributes().at(0).value().toString() == "destination"){
                            destination = reader.attributes().at(2).value().toString();
                            if(destination == ""){
                                destination = "UnknownBarCode";
                            }
                            if(reader.attributes().at(1).value().contains("1536")){
                                columns = 48;
                                rows = 32;
                                destinationFormat = 1536;
                            }else if(reader.attributes().at(1).value().contains("384")){
                                columns = 24;
                                rows = 16;
                                destinationFormat = 384;
                            }else{
                                qDebug() << "Unknown well number in destination plate name";
                            }
                        }
                        reader.readNext();
                    }

                    //set transfer title
                    QString both;
                    if(sourceFormat != destinationFormat){
                        both = "Transfer: " + source + " (" + QString::number(sourceFormat) + " well) → " + destination + " (" + QString::number(destinationFormat) + " well)";
                    }else{
                        both = "Transfer: " + source + " → " + destination;
                    }
                    ui->label->setText(both);
                    reader.readNext();

                    //Fill table with empty QTableWidgetItems and set color to gray
                    ui->tableWidget->setColumnCount(columns);
                    ui->tableWidget->setRowCount(rows);

                    QStringList headers = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF"};
                    ui->tableWidget->setVerticalHeaderLabels(headers);

                    for (int i = 0; i < rows; i++){
                        for (int j = 0; j < columns; j++){
                            ui->tableWidget->setItem(i, j, new QTableWidgetItem);
                            ui->tableWidget->item(i, j)->setBackground(Qt::gray);
                        }
                    }
                }
                //set transfered wells to green
                if(reader.name() == "printmap"){
                    while(reader.readNextStartElement()){
                        if(reader.name() == "w"){
                            int row = reader.attributes().at(4).value().toInt();
                            int col = reader.attributes().at(5).value().toInt();
                            ui->tableWidget->item(row, col)->setBackground(Qt::green);

                        }
                        reader.readNext();
                    }
                }
                reader.readNext();

                //Get skipped wells from tranfer files
                if(reader.name() == "skippedwells"){
                    while(reader.readNextStartElement()){
                        if(reader.name() == "w"){
                            int row = reader.attributes().at(1).value().toInt();
                            int col = reader.attributes().at(2).value().toInt();
                            QString toolTip = reader.attributes().at(0).value().toString();

                            toolTip.append(": ");
                            toolTip.append(reader.attributes().last().value().toString());

                            ui->tableWidget->item(row, col)->setBackground(Qt::red);
                            ui->tableWidget->item(row,col)->setToolTip(toolTip);

                        }
                        reader.readNext();
                    }
                }
                else{
                    reader.skipCurrentElement();
                }
            }
        }
        else
            reader.raiseError(QObject::tr("Incorrect file"));
    }
}

void Widget::caluclateMinMax(QString path)
{

    //Open XML file
    QFile file(path);
    QXmlStreamReader reader(&file);

    if(!file.open(QFile::ReadOnly | QFile::Text)){
        qDebug() << "Cannot read file" << file.errorString();
        exit(0);
    }

    //Survey files:
    if (reader.readNextStartElement()) {
        if (reader.name() == "platesurvey"){

            //Get row and column attributes
            rows = reader.attributes().at(7).value().toInt();
            columns = reader.attributes().at(8).value().toInt();

            Vmin = 20;
            Vmax = 0;
            Dmin = 100;
            Dmax = 0;

            //Get volume and % DMSO attributes
            while(reader.readNext()){
                if(reader.atEnd()) break;

                if(reader.name() == "w"){
                    double volume = reader.attributes().at(3).value().toDouble();
                    double DMSOcontent = reader.attributes().at(11).value().toDouble();

                    if(volume > 0){
                        if(volume < Vmin) Vmin = volume;
                        if(volume > Vmax) Vmax = volume;
                    }
                    if(DMSOcontent > 0){
                        if(DMSOcontent < Dmin) Dmin = DMSOcontent;
                        if(DMSOcontent > Dmax) Dmax = DMSOcontent;
                    }

                }
                reader.readNext();
            }
        }
    }
}

void Widget::showLegend(bool show)
{
    ui->label_2->setVisible(show);
    ui->label_3->setVisible(show);
    ui->label_4->setVisible(show);
    ui->labelMin->setVisible(show);
    ui->labelMax->setVisible(show);
}

QString Widget::getQuadrant(QString path, bool src)
{

    //Open XML file
    QFile file(path);
    QXmlStreamReader reader(&file);

    if(!file.open(QFile::ReadOnly | QFile::Text)){
        qDebug() << "Cannot read file" << file.errorString();
        exit(0);
    }

    int row = -1;
    int col = -1;

    //Get rows and columns of the first well to determine quadrant
    while(reader.readNext()){
        if(reader.atEnd()) break;

        if(reader.name() == "printmap"){
            reader.readNextStartElement();
            if(reader.name() == "w"){
                if(src){
                    row = reader.attributes().at(1).value().toInt();
                    col = reader.attributes().at(2).value().toInt();
                }else{
                    row = reader.attributes().at(4).value().toInt();
                    col = reader.attributes().at(5).value().toInt();
                }
            }
        }
        reader.readNext();

    }

    if(row == 0 && col == 0){
        return "Q1";
    }else if(row == 0 && col == 1){
        return "Q2";
    }else if(row == 1 && col == 0){
        return "Q3";
    }else if (row == 1 && col == 1){
        return "Q4";
    }else{
        return nullptr;
    }
}

void Widget::on_pushButton_clicked()
{
    desktop = QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).at(0);

    inputFiles = QFileDialog::getOpenFileNames(this, tr("Select files"),
                                               desktop, tr("XML files (*.xml *.results)"));

    QString type = "";
    plateRelations = "Source Quadrant;Source Barcode;Destination Barcode;Destination Quadrant\n";

    //Open all selected files to get type (survey or transfer) for QListWidget
    for (int i = 0; i < inputFiles.size(); ++i){
        QFile file(inputFiles.at(i));
        QXmlStreamReader reader(&file);

        if(!file.open(QFile::ReadOnly | QFile::Text)){
            qDebug() << "Cannot read file" << file.errorString();
            exit(0);
        }

        if (reader.readNextStartElement()) {
            if (reader.name() == "platesurvey"){
                type = "Survey";
            }else if (reader.name() == "transfer"){
                type = "Transfer";

                //If it's a tranfer, get the source and destination format form the Echo plate name
                while(reader.readNextStartElement()){
                    if(reader.name() == "plateInfo"){
                        int sourceFormat = 0;
                        int destinationFormat = 0;
                        while(reader.readNextStartElement()){

                            if(reader.attributes().at(0).value().toString() == "source"){
                                source = reader.attributes().at(2).value().toString();
                                if(reader.attributes().at(1).value().contains("1536")){
                                    sourceFormat = 1536;
                                }else if (reader.attributes().at(1).value().contains("384")){
                                    sourceFormat = 384;
                                }else{
                                    qDebug() << "Unknown well number in source plate name";
                                }
                            }else if(reader.attributes().at(0).value().toString() == "destination"){
                                destination = reader.attributes().at(2).value().toString();
                                if(destination == ""){
                                    destination = "UnknownBarCode";
                                }
                                if(reader.attributes().at(1).value().contains("1536")){
                                    destinationFormat = 1536;
                                }else if(reader.attributes().at(1).value().contains("384")){
                                    destinationFormat = 384;
                                }else{
                                    qDebug() << "Unknown well number in destination plate name";
                                }
                            }
                            reader.readNext();
                        }

                        //Append the plate relations and quadrant to the CSV export string
                        if(sourceFormat < destinationFormat){
                            plateRelations.append(";" + source + ";" + destination + ";" + getQuadrant(inputFiles.at(i), false) + "\n");
                        }else if(sourceFormat > destinationFormat){
                            plateRelations.append(getQuadrant(inputFiles.at(i), true) +";" + source + ";" + destination + ";" + "\n");
                        }else{
                            plateRelations.append(";" + source + ";" + destination + ";" + "\n");
                        }

                    }
                }
            }
        }

        //Generate the plate name that appears in the list widget
        QString fileName = inputFiles.at(i).mid(inputFiles.at(i).lastIndexOf('/'));
        QString listName;

        if(fileName.contains("xml")){
            listName = fileName.mid(1, fileName.size() - 5);
        }else{
            QString barcode = fileName.section('_', 1, 1);
            QString date = fileName.section('_', 2, 2);

            listName = barcode + "_" +  date + "_" + type;
        }

        ui->listWidget->addItem(listName);
        ui->listWidget->setCurrentRow(0);
    }
}

void Widget::on_listWidget_currentRowChanged(int currentRow)
{
    parseLog(inputFiles.at(currentRow));
}

void Widget::on_radioButton_Volume_clicked()
{
    ui->radioButton_Volume->setChecked(true);
    parseLog(inputFiles.at(ui->listWidget->currentRow()));
}

void Widget::on_radioButton_DMSO_clicked()
{
    ui->radioButton_DMSO->setChecked(true);
    parseLog(inputFiles.at(ui->listWidget->currentRow()));
}


Widget::~Widget()
{
    delete ui;
}

void Widget::on_button_survey_clicked()
{
    QString survey;

    if(ui->radioButton_DMSO->isChecked()){
        survey = "_survey_DMSO";
    }else{
        survey = "_suvey_volume";
    }


    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save file as"), desktop + "/" + barcode + survey,
                                                    tr("CSV (*.csv);;All Files (*)"));

    //Export volume or DMSO data
    QFile data(fileName);
    if(data.open(QIODevice::WriteOnly |QIODevice::Truncate))
    {
        QDataStream output(&data);
        output.writeRawData(surveyExport.toUtf8(), surveyExport.size());
    }

}

void Widget::on_button_relations_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save file as"), desktop + "/" + "plate relations",
                                                    tr("CSV (*.csv);;All Files (*)"));


    QFile data(fileName);
    if(data.open(QIODevice::WriteOnly |QIODevice::Truncate))
    {
        QDataStream output(&data);
        output.writeRawData(plateRelations.toUtf8(), plateRelations.size());
    }
}

void Widget::on_tableWidget_cellClicked(int row, int column)
{
    if(row == 0 and column == 0 and inputFiles.size() == 0){
        on_pushButton_clicked();
    }
}

void Widget::restart()
{
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

void Widget::info()
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText("Version: 1.0 ");
    msgBox.setInformativeText("Release date: 2020-06-15");
    msgBox.exec();
}

