#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QXmlStreamReader>
#include <QMainWindow>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pushButton_clicked();
    void on_listWidget_currentRowChanged(int currentRow);
    void on_radioButton_Volume_clicked();
    void on_radioButton_DMSO_clicked();
    void on_button_survey_clicked();
    void on_button_relations_clicked();
    void on_tableWidget_cellClicked(int row, int column);
    void restart();
    void info();

private:
    Ui::Widget *ui;
    void parseLog(QString path);
    QString desktop;
    QString source;
    QString destination;
    QStringList inputFiles;
    int rows, columns;
    void caluclateMinMax(QString path);
    double Vmin, Vmax, Dmin, Dmax;
    void showLegend(bool);
    QString surveyExport;
    QString dmsoCSV;
    QString barcode;
    QString plateRelations;
    QString getQuadrant(QString path, bool src);
};

#endif // WIDGET_H
